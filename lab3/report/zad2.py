import cv2
import numpy as np
from matplotlib import pyplot as plt

def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

imgBoats = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab3/slike/boats.bmp',0)
imgBaboon = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab3/slike/baboon.bmp',0)
imgAirplane = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab3/slike/airplane.bmp',0)


ret1,th1 = cv2.threshold(imgBoats,127,255,cv2.THRESH_BINARY)

ret2,th2 = cv2.threshold(imgBaboon,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

ret3,th3 = cv2.threshold(imgAirplane, 120, 255, cv2.THRESH_BINARY_INV) 

ret1,th4 = cv2.threshold(imgBoats,127,255,cv2.THRESH_TRUNC)

ret2,th5 = cv2.threshold(imgBaboon,0,255,cv2.THRESH_TOZERO)

ret3,th6 = cv2.threshold(imgAirplane, 120, 255, cv2.THRESH_TOZERO_INV) 

cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab3/report/treshbinary.bmp",th1)
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab3/report/treshbinary+otsu.bmp",th2)
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab3/report/treshbinaryinv.bmp",th3)
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab3/report/treshtrunc.bmp",th4)
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab3/report/treshtozero.bmp",th5)
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab3/report/treshtozeroinv.bmp",th6)
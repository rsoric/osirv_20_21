import numpy as np
import cv2

slika = cv2.imread('slike/baboon.bmp')

slika_blue = slika.copy()
slika_blue[:,:,1] = 0
slika_blue[:,:,2] = 0

cv2.imwrite("plava.jpg", slika_blue)

slika_crvena = slika.copy()
slika_crvena[:,:,0] = 0
slika_crvena[:,:,1] = 0

cv2.imwrite("crvena.jpg", slika_crvena)

slika_zelena = slika.copy()
slika_zelena[:,:,0] = 0
slika_zelena[:,:,2] = 0 

cv2.imwrite("zelena.jpg", slika_zelena)
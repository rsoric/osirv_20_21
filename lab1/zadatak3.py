import numpy as np
import cv2

slika = cv2.imread('slike/lenna.bmp')
slika_border = cv2.copyMakeBorder(slika,10,10,10,10,cv2.BORDER_CONSTANT)

cv2.imwrite("slika_border.jpg", slika_border)
import numpy as np
import cv2

slika = cv2.imread('slike/BoatsColor.bmp')

slika_v = slika.copy()
slika_v = slika_v[:,::2,:]

slika_h = slika.copy()
slika_h = slika_h[::2,:,:]

slika_vh = slika.copy()
slika_vh = slika_vh[::2,::2,:]

cv2.imwrite("slika_v.jpg", slika_v)
cv2.imwrite("slika_h.jpg", slika_h)
cv2.imwrite("slika_vh.jpg", slika_vh)
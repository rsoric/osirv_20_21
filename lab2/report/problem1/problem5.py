import numpy as np
import math
import cv2

#implementacaija kvantiziranja u funkciji prema upute koje su dane
def quantize(image, q):
    d = 2**(8-q)
    output = np.zeros((image.shape[0], image.shape[1]))
    for i in range(0, image.shape[0]):
        for j in range(0, image.shape[1]):
            output[i,j] = (np.floor(image[i,j]/d) + 0.5) * d
    return output

image = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/BoatsColor.bmp',0)
image = image.astype(np.float32)
q = [1,2,3,4,5,6,7,8]

for qCurrent in q:
    
    quantizedImage = quantize(image,qCurrent)
    quantizedImage = quantizedImage.astype(np.uint8)
    cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem5/boats_q"+str(qCurrent)+".bmp",quantizedImage)
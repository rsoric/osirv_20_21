import numpy as np
import cv2

def addBlackBorder(image,bordersize):

    borderedimage = cv2.copyMakeBorder(image, bordersize, bordersize, bordersize, bordersize, cv2.BORDER_CONSTANT)
    return borderedimage
    
    #čitajući tekst zadatka došao sam do dijela u kojem piše:
    #"Učitajte sliku i dodajte crni okvir oko slike, ali na način da ne
    #prepisujete dijelove slike, već da dodate okvir na rub slike."
    #nisam uspio naći način da koristim vstack i hstack funkcije
    #ili neki drugi način da dodam redove i stupce
    #koristio sam defaultnu funkciju iz biblioteke cv2 koja dodaje okvir

    #kod ispod je način na koji sam pokušao riješiti zadatak

    #imageWidth = np.shape(image)[0]
    #imageHeight = np.shape(image)[1]
    
    #topbottomborder = np.zeros([imageWidth,bordersize,3],dtype=np.uint8)
    #leftrightborder = np.zeros([bordersize,(bordersize+bordersize+imageHeight),3],dtype=np.uint8)
    
    #borderedimage = np.vstack((topbottomborder,image))
    #borderedimage= np.vstack((borderedimage,topbottomborder))
    #borderedimage= np.hstack((leftrightborder,borderedimage))
    #borderedimage= np.hstack((borderedimage,leftrightborder))

    

image = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/lenna.bmp')
borderedimage = addBlackBorder(image,250)
cv2.imshow("Spojena slika",borderedimage)
cv2.waitKey(0)
cv2.destroyAllWindows()
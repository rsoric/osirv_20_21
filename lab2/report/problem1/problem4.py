import numpy as np
import cv2

image1 = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/airplane.bmp',0)
ret,image1_tresh63 = cv2.threshold(image1,63,255,cv2.THRESH_BINARY)
ret,image1_tresh127 = cv2.threshold(image1,127,255,cv2.THRESH_BINARY)
ret,image1_tresh191 = cv2.threshold(image1,191,255,cv2.THRESH_BINARY)

image2 = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/baboon.bmp',0)
ret,image2_tresh63 = cv2.threshold(image2,63,255,cv2.THRESH_BINARY)
ret,image2_tresh127 = cv2.threshold(image2,127,255,cv2.THRESH_BINARY)
ret,image2_tresh191 = cv2.threshold(image2,191,255,cv2.THRESH_BINARY)

image3 = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/lenna.bmp',0)
ret,image3_tresh63 = cv2.threshold(image3,63,255,cv2.THRESH_BINARY)
ret,image3_tresh127 = cv2.threshold(image3,127,255,cv2.THRESH_BINARY)
ret,image3_tresh191 = cv2.threshold(image3,191,255,cv2.THRESH_BINARY)

cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem4/airplane_tresh63.png",image1_tresh63)
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem4/airplane_tresh127.png",image1_tresh127)
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem4/airplane_tresh191.png",image1_tresh191)

cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem4/baboon_tresh63.png",image2_tresh63)
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem4/baboon_tresh127.png",image2_tresh127)
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem4/baboon_tresh191.png",image2_tresh191)

cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem4/lenna_tresh63.png",image3_tresh63)
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem4/lenna_tresh127.png",image3_tresh127)
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem4/lenna_tresh191.png",image3_tresh191)
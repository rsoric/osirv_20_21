import numpy as np
import cv2

image1 = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/airplane.bmp')
image1 = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)

image2 = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/baboon.bmp')
image2 = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)

image3 = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/lenna.bmp')
image3 = cv2.cvtColor(image3, cv2.COLOR_BGR2GRAY)

cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem3/airplane_invert.png",cv2.bitwise_not(image1))
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem3/baboon_invert.png",cv2.bitwise_not(image2))
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/problem3/lenna_invert.png",cv2.bitwise_not(image3))
#invertanje se vrši pomoću bitwise not operacije
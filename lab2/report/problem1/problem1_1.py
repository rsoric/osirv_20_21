import numpy as np
import cv2

image1 = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/airplane.bmp')
image2 = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/baboon.bmp')
image3 = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/lenna.bmp')

image1_cropped = image1[0:300,0:300]
image2_cropped = image2[0:300,0:300]
image3_cropped = image3[0:300,0:300]

#slike su croppane na jednostavan način
#sa uglatim zagradama (pošto je array muteable data type) smo izabrali samo ona dio arraya koji nas zanima

combined_image = np.hstack((image1_cropped,image2_cropped))
combined_image = np.hstack((combined_image,image3_cropped))

cv2.imshow("Spojena slika",combined_image)
cv2.waitKey(0)
cv2.destroyAllWindows()
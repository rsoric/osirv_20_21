import cv2
import copy
import numpy as np
from math import sin, cos, pi

img = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/baboon.bmp', 0)
rows,cols = img.shape

#resizeanje
scale_percent = 25
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)
dim = (width, height)
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

#pravljenje za iteriranje kroz brojne rotacije
rotations = np.arange(30,360,30)
result = np.copy(resized)

for rotation in rotations:
  M = cv2.getRotationMatrix2D((width/2,height/2),rotation,1)
  dst = cv2.warpAffine(resized,M,(width,height))
  result = np.hstack((result,dst))

cv2.imshow("Baboon rotated", result)
cv2.waitKey(0)
cv2.destroyAllWindows()
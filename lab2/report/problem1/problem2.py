import numpy as np
import cv2

#implementacija konvolucije koja nam je dana
def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 


image = cv2.imread('/home/student/Documents/osirv labosi/osirv_20_21/lab2/slike/lenna.bmp')
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

#kerneli s wikipedije
kernel_Identity = np.array([
    [0,0,0],
    [0,1,0],
    [0,0,0]], dtype=np.float32)

kernel_EdgeDetection = np.array([
    [-1,-1,-1],
    [-1,8,-1],
    [-1,-1,-1]], dtype=np.float32)

kernel_Sharpen = np.array([
    [0,-1,0],
    [-1,5,-1],
    [0,-1,0]], dtype=np.float32)

kernel_BoxBlur = np.array([
    [1,1,1],
    [1,1,1],
    [1,1,1]], dtype=np.float32)*(1/9)

kernel_GaussianBlur5x5 = np.array([
    [1,4,6,4,1],
    [4,16,24,16,4],
    [6,24,36,24,6],
    [4,16,24,16,4],
    [1,4,6,4,1]], dtype=np.float32)*(1/256)
    
kernel_UnsharpMask5x5 = np.array([
    [1,4,6,4,1],
    [4,16,24,16,4],
    [6,24,-476,24,6],
    [4,16,24,16,4],
    [1,4,6,4,1]], dtype=np.float32)*(1/256)


cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/convolution/identity.png",convolve(image,kernel_Identity))
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/convolution/edgedetection.png",convolve(image,kernel_EdgeDetection))
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/convolution/sharpen.png",convolve(image,kernel_Sharpen))
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/convolution/boxblur.png",convolve(image,kernel_BoxBlur))
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/convolution/gaussianblur.png",convolve(image,kernel_GaussianBlur5x5))
cv2.imwrite("/home/student/Documents/osirv labosi/osirv_20_21/lab2/convolution/unsharpmask.png",convolve(image,kernel_UnsharpMask5x5))